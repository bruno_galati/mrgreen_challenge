class MrGreen {
    constructor(customConfig) {
        let defaultConfig = {
            NumberSlotsServer: 2,
            startWithServers: 4,
            availableApps: []
        }
        //merge configs
        const config = { ...defaultConfig,
            ...customConfig
        }

        this.runningServers = []
        this.NumberSlotsServer = config.NumberSlotsServer
        this.startWithServers = config.startWithServers
        this.availableApps = config.availableApps

        //add empty servers to clusters
        for (let i = 1; i <= parseInt(this.startWithServers); i++) {
            this.add_server()
        }

        //build menu for apps
        let appsShelf = document.querySelector('.apps_shelf')
        let ulApps = document.createElement('ul')
        this.availableApps.map((item) => {
            let liApp = document.createElement('li')
            let addAppBtn = document.createElement('div')
            let removeAppBtn = document.createElement('div')

            addAppBtn.classList.add('add_app')
            addAppBtn.setAttribute('data-initials', item.initials)
            addAppBtn.setAttribute('data-className', item.className)
            addAppBtn.setAttribute('data-name', item.name)
            addAppBtn.setAttribute('data-id', item.id)
            addAppBtn.classList.add(item.className)
            addAppBtn.innerHTML = '&plus;'

            removeAppBtn.classList.add('remove_app')
            removeAppBtn.setAttribute('data-initials', item.initials)
            removeAppBtn.setAttribute('data-className', item.className)
            removeAppBtn.setAttribute('data-name', item.name)
            removeAppBtn.setAttribute('data-id', item.id)
            removeAppBtn.innerHTML = '&minus;'

            liApp.classList.add('border-' + item.className)
            liApp.innerHTML = item.name
            liApp.appendChild(addAppBtn)
            liApp.appendChild(removeAppBtn)
            ulApps.appendChild(liApp)
        })
        appsShelf.appendChild(ulApps)

        // //listen to events
        document.addEventListener('click', (e) => {
            //add new server
            if (e.target.matches('.add_server')) {
                this.add_server()
                this.render()
            }
            //remove server
            if (e.target.matches('.remove_server')) {
                this.delete_server()
                this.render()
            }
            //remove selected server
            if (e.target.matches('.server')) {
                console.log(e.target)
                this.delete_server(e.target.dataset.id)
                this.render()
            }
            //add app
            if (e.target.matches('.add_app')) {
                this.add_app(e.target.dataset.id)
                this.render()
            }
            //remove app
            if (e.target.matches('.remove_app')) {
                let appToRemove = e.target.dataset.id
                this.delete_app(appToRemove)
                this.render()
            }
        }, false)
    }

    add_server() {
        let server = {
            'id': Date.now() + this.runningServers.length,
            'slots': {}
        }
        //start from
        let qtdSlot = 1
        // create slots on server
        while (qtdSlot <= this.NumberSlotsServer) {
            server.slots[qtdSlot] = null
            qtdSlot++
        }
        this.runningServers.push(server)
    }

    delete_app(appToRemove) {
        let tempRemovalArray = []
        this.runningServers.map((server) => {
            let allSlots = Object.keys(server.slots)
            //add apps to temp array
            allSlots.map((slot) => {
                if (server.slots[slot] && appToRemove == server.slots[slot].id) {
                    tempRemovalArray.push(server.slots[slot].created)
                }
            })
        })
        //find the most recent created app for removal
        let mostRecent = Math.max.apply(Math, tempRemovalArray)
        this.runningServers.map((server) => {
            let allSlots = Object.keys(server.slots)
            allSlots.map((slot) => {
                // if slot is the same of app for removal and the is the most recent created
                if (server.slots[slot] && server.slots[slot].created == mostRecent && appToRemove == server.slots[slot].id) {
                    server.slots[slot] = null
                }
            })
        })
    }

    add_app(id) {
        //start from
        let qtdSlot = 1
        //next servers with empty slots
        let nextServers = []
        let nextSlot = {}
        let appSelected = []
        let obj = {}
        while (qtdSlot <= this.NumberSlotsServer) {
            //find servers with empty slot
            nextServers = this.runningServers.filter((e) => {
                return !e.slots[qtdSlot]
            })
            //grab the first server with empty slot
            if (nextServers.length > 0) {
                //the first empty slot
                nextSlot = nextServers[0]
                appSelected = this.availableApps.filter((el) => {
                    return el.id == id
                })
                if (nextSlot.slots) {
                    obj = {
                        id: appSelected[0].id,
                        name: appSelected[0].name,
                        initials: appSelected[0].initials,
                        className: appSelected[0].className,
                        created: Date.now() + qtdSlot,
                        stamp: Date.now()
                    }
                    nextSlot.slots[qtdSlot] = obj
                    break
                }
            }
            qtdSlot++
        }
    }

    delete_server(idServer) {
        let server
        //if server is specified
        if (idServer) {
            let serverRemove = this.runningServers.filter((el) => {
                if (el.id == idServer) {
                    return true
                }
            })
            this.runningServers = this.runningServers.filter((el) => {
                if (el.id == idServer) {
                    return false
                }
                return true
            })
            //remove with id
            server = serverRemove[0]
        } else {
            //remove last server
            server = this.runningServers.pop()
        }
        //if have apps inside slots
        if (server.slots) {
            //grab the apps
            let slotsKeys = Object.keys(server.slots)
            slotsKeys.map((el) => {
                //if have any app inside this server
                if (server.slots[el] && server.slots[el].id) {
                    //try add in next available slot
                    this.add_app(server.slots[el].id)
                }
            })
        }
    }

    render() {
        let server
        let cluster = document.getElementById('cluster')
        cluster.innerHTML = ''
        this.runningServers.map((item) => {
            server = document.createElement('div')
            server.classList.add('server')
            server.setAttribute('data-id', item.id)
            let availableSlot = Object.keys(item.slots)
            availableSlot.map((slot) => {
                if (item.slots[slot]) {
                    let timePassed = Date.now() - (item.slots[slot].stamp)
                    let timeFormat = this.formatTime(timePassed)
                    let str = (timeFormat.time || '') + ' ' + timeFormat.operator
                    let appEl = document.createElement('div')
                    appEl.classList.add(item.slots[slot].className ? item.slots[slot].className : '')
                    appEl.innerHTML = ('<b>' + (item.slots[slot].initials ? item.slots[slot].initials : '') + '</b>' + (item.slots[slot].name ? item.slots[slot].name : '') + '<small> ' + str + '</small>')
                    server.appendChild(appEl)
                }
            })
            cluster.appendChild(server)
        })
    }
    //format date
    formatTime(timePassed) {
        let t = Math.round(timePassed / 1000)
        let ret = ''
        let operator = 'second' + ((t > 1) ? 's' : '') + ' ago'

        if (t == 0) {
            operator = 'Right now'
        } else if (t >= 60) {
            t = Math.round(t / 60)
            operator = 'minute' + ((t > 1) ? 's' : '') + ' ago'
            if (t >= 60) {
                t = Math.round(t / 60)
                operator = 'hour' + ((t > 1) ? 's' : '') + ' ago'
            }
        }
        return {
            time: t,
            operator: operator
        }
    }

}

const availableApps = [{
        id: 1,
        name: 'Hadoop',
        initials: 'Hd',
        className: 'bg-pink'
    },
    {
        id: 2,
        name: 'Rails',
        initials: 'Ra',
        className: 'bg-purple'
    },
    {
        id: 3,
        name: 'Chronos',
        initials: 'Ch',
        className: 'bg-blue'
    },
    {
        id: 4,
        name: 'Storm',
        initials: 'St',
        className: 'bg-waterblue'
    },
    {
        id: 5,
        name: 'Spark',
        initials: 'Sp',
        className: 'bg-green'
    }
]
let app = new MrGreen({
    availableApps
})
app.render()